const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV === 'production'){
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);

}else{
    if (process.env.NODE_ENV === 'staging'){
        console.log('xxxxxxxxxxxxxxxx');
        const options ={
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailCOnfig = sgTransport(options);

    } else {
        // all emails are catched by etheeal.email
        mailCOnfig = {
            host: 'sntp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd
            }
        };
    }
}

// const mailConfig = {
//     host: 'smtp.ethereal.email',
//     port: 587,
//     auth: {
//         user: 'electa.stiedemann@ethereal.email',
//         pass: 'vyw5tvKBHWY2mjDK2T'
//     }
// };

module.exports = nodemailer.createTransport(mailConfig);