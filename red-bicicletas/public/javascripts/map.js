var map = L.map('main_map').setView([9.5058928,-83.4838361], 12);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([9.4842441,-83.4867676]).addTo(map)
//     .bindPopup('Chirripó Costa Rica')
//     .openPopup();

// L.marker([9.4646369,-83.6106298]).addTo(map)
//     .bindPopup('San Gerardo Costa Rica')
//     .openPopup();

// L.marker([9.417918, -83.469546]).addTo(map)
//     .bindPopup('Meta 1')
//     .openPopup();

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})